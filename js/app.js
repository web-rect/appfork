

// блог переключалки / faq

const $checks = document.querySelectorAll('.check-in-js');


if ($checks) {
    $checks.forEach(item => {
        
        item.addEventListener('click', (e) => {
   
            if (item.getAttribute('active')) {
                
                if (e.target.closest('.title-js')) {
                 
                    $checks.forEach(elem => {
                        elem.classList.remove('active');
                        elem.removeAttribute('active');
                    });
                    item.classList.remove('active');
                    item.removeAttribute('active');
                }
               
            } else {
                if (e.target.closest('.title-js')) {
                    $checks.forEach(elem => {
                        elem.classList.remove('active');
                        elem.removeAttribute('active');
                    });
                    item.classList.add('active');
                    item.setAttribute('active', 'active');
                }
                
            }
        });
    });
}

// кнопка при скролле 

const $header = document.querySelector('.header');

if ($header) {
    window.addEventListener('scroll', () => {
        const srcl =  window.scrollY;
        if (srcl > 10) {
            $header.classList.add('active');
        }
        if (srcl < 10) {
            $header.classList.remove('active');
        }
    });
} 

// выпадающее меню

const $btnMenu = document.querySelector('.btn-menu');

if ($btnMenu) {
    
    $btnMenu.addEventListener('click', e => {

        if (e.target.closest('.btn-menu span')) {
            $btnMenu.classList.toggle('active');
        }
    });
}

// курсор 

var cursor = {
    delay: 8,
    _x: 0,
    _y: 0,
    endX: (window.innerWidth / 2),
    endY: (window.innerHeight / 2),
    cursorVisible: true,
    cursorEnlarged: false,
    $dot: document.querySelector('.cursor-bg-dot'),
    $outline: document.querySelector('.cursor-bg'),
    
    init: function() {
        // Set up element sizes
        this.dotSize = this.$dot.offsetWidth;
        this.outlineSize = this.$outline.offsetWidth;
        
        this.setupEventListeners();
        this.animateDotOutline();
    },
    
    setupEventListeners: function() {
        var self = this;
        
        // Anchor hovering
        document.querySelectorAll('a').forEach(function(el) {
            el.addEventListener('mouseover', function() {
                self.cursorEnlarged = true;
                self.toggleCursorSize();
            });
            el.addEventListener('mouseout', function() {
                self.cursorEnlarged = false;
                self.toggleCursorSize();
            });
        });
        
        // Click events
        document.addEventListener('mousedown', function() {
            self.cursorEnlarged = true;
            self.toggleCursorSize();
        });
        document.addEventListener('mouseup', function() {
            self.cursorEnlarged = false;
            self.toggleCursorSize();
        });
  
        document.addEventListener('mousemove', function(e) {
            // Show the cursor
            self.cursorVisible = true;
            self.toggleCursorVisibility();

            // Position the dot
            self.endX = e.pageX;
            self.endY = e.pageY;
            self.$dot.style.top = self.endY + 'px';
            self.$dot.style.left = self.endX + 'px';
        });
        
        // Hide/show cursor
        document.addEventListener('mouseenter', function(e) {
            self.cursorVisible = true;
            self.toggleCursorVisibility();
            self.$dot.style.opacity = 1;
            self.$outline.style.opacity = 1;
        });
        
        document.addEventListener('mouseleave', function(e) {
            self.cursorVisible = true;
            self.toggleCursorVisibility();
            self.$dot.style.opacity = 0;
            self.$outline.style.opacity = 0;
        });
    },
    
    animateDotOutline: function() {
        var self = this;
        
        self._x += (self.endX - self._x) / self.delay;
        self._y += (self.endY - self._y) / self.delay;
        self.$outline.style.top = self._y + 'px';
        self.$outline.style.left = self._x + 'px';
        
        requestAnimationFrame(this.animateDotOutline.bind(self));
    },
    
    toggleCursorSize: function() {
        var self = this;
        
        if (self.cursorEnlarged) {
            self.$dot.style.transform = 'translate(-50%, -50%) scale(0.75)';
            self.$outline.style.transform = 'translate(-50%, -50%) scale(1.5)';
        } else {
            self.$dot.style.transform = 'translate(-50%, -50%) scale(1)';
            self.$outline.style.transform = 'translate(-50%, -50%) scale(1)';
        }
    },
    
    toggleCursorVisibility: function() {
        var self = this;
        
        if (self.cursorVisible) {
            self.$dot.style.opacity = 1;
            self.$outline.style.opacity = 1;
        } else {
            self.$dot.style.opacity = 0;
            self.$outline.style.opacity = 0;
        }
    }
}
cursor.init();

// прогресс бар

window.onscroll = function() { ScrollIndicator() };

function ScrollIndicator() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.querySelector('.progress_bar').style.height = scrolled + "%";
}


// слайдеры

const $gamblingTitleLink = document.querySelector('.title-link-js');
const $contentsSliderJs = document.querySelectorAll('.contents-slider-js');
const $animation_addAll = document.querySelectorAll('.animation_add');


if ($contentsSliderJs) {
    $contentsSliderJs.forEach(content => {

        const $gamblingBlock = content.querySelectorAll('.gambling-block');
        const $gamblingLinkA = content.querySelectorAll('.title-link-js a');
      
        let count = +content.dataset.count;
      
        let timer = 5000;
    
        const sliderActive = () => {
            $gamblingLinkA.forEach((link, i) => {
                link.addEventListener('click', e => {
                    count = i;
                    funSlider();
                    
                });
            });
        }
    
        const funSlider = () => {
     
            content.querySelectorAll('.animation_add').forEach(elem => elem.classList.remove('fadeInUpCustom'));
       

            $gamblingLinkA[count].classList.add('active');
            $gamblingBlock[count].classList.add('active');
    
            for (let i = 0; i < $gamblingLinkA.length; i++) {
    
                for (let j = 0; j < $gamblingBlock.length; j++) {
                    
                    $gamblingLinkA[i].classList.remove('active');
                    $gamblingBlock[j].classList.remove('active');
    
                    $gamblingLinkA[count].classList.add('active');
                    $gamblingBlock[count].classList.add('active');
    
                    $gamblingBlock[count].querySelectorAll('.animation_add').forEach(elem => {
                        setTimeout(() => {
                            elem.classList.add('fadeInUpCustom');
                        }, elem.dataset.wowDelay);
                      
                    });
                    
    
                    clearTimeout(times);
                    times = setInterval(() => {
                        funSlider();
                    }, timer);
                }
            }
    
            if ($gamblingLinkA.length - 1 === count) {
                count = 0;
            } else {
                count += 1;
            }
    
        }
      
        let times = setInterval(() => {
            funSlider();
        }, timer);
        funSlider();
        sliderActive();
    })
 
}


// // подсчет ширины портфолио

// const $marquee__content = document.querySelectorAll('.marquee__content'),
// $marquee_contentWrap = document.querySelector('.marquee_content-wrap'),
// $listInline = document.querySelectorAll('.list-inline');

// if ($marquee__content) {

//     const resizeStart = () => {
//         let sum;
//         $listInline.forEach(ul => {
    
//             const allLi = ul.querySelectorAll('li');
    
//             allLi.forEach(li => {
//                 sum = (li.clientWidth + 50) *  allLi.length * $listInline.length;
//             })
    
//         });
        
//         $marquee_contentWrap.style.width = sum * $marquee__content.length + 'px';
     
        
//     }
  
//     window.addEventListener('resize', resizeStart);
//     window.addEventListener('DOMContentLoaded', resizeStart);

// }

// модалка

const $modalFormQuiz = document.querySelector('.modal-form-quiz'),
$btnModalJs = document.querySelectorAll('.btn-modal-js'),
$btnBlack = document.querySelectorAll('.btn-black');


if ($modalFormQuiz) {
    $btnModalJs.forEach(linkBtn => {
        linkBtn.addEventListener('click', e => {
            e.preventDefault();
            $modalFormQuiz.classList.add('active');
        });
    })
    $modalFormQuiz.addEventListener('click', e => {
        
        if (e.target.closest('.modal-form-quiz-bg') || e.target.closest('.close')) {
            $modalFormQuiz.classList.remove('active');
        }
    })

    let stcount = 0;
    const $items = $modalFormQuiz.querySelectorAll('.items'); 
    const $itemsError = document.querySelectorAll('.text-error'); 
    const slideGoBtn = document.querySelectorAll('.btn-js-go');
    const btnReturnОs = document.querySelector('.btn-return-js');

    let $checkAllInp = "";
    let $allInputVal = "";
    let $textarea = "";

    slideGoBtn.forEach((link, i) => {
        link.addEventListener('click', e => {

            let $itemsChekedInput = $items[0].querySelectorAll('input:checked').length;
            let $itemsValueInput = $items[2].querySelectorAll('input');


            errorRemove();

     
            if (e.target.closest('.bt1')) {
                if ($itemsChekedInput > 0) {
                    $items[0].querySelectorAll('input:checked').forEach(val => {
                        $checkAllInp += val.value + ', ';
                    })
                    console.log($items); 
                  
                    countplus();
                    errorRemove();
           
                }  else {
                    errorAdd();
                }
            }

            if (e.target.closest('.bt2')) {
                $textarea = $items[1].querySelector('textarea').value;
                countplus();
                errorRemove();
            }
        
            if (e.target.closest('.bt3')) {
                if ($itemsValueInput[0].value.length > 1 && $itemsValueInput[1].value.length > 1 || $itemsValueInput[2].value.length > 1) {
                    
                    $itemsValueInput.forEach(inps => {
                        $allInputVal += inps.value + ', ';
                    })

                    const formData = new FormData();
                    formData.append('type', $checkAllInp);
                    formData.append('textarea', $textarea);
                    formData.append('contact', $allInputVal);
            
                    const ftsend = (data) => {
                        fetch('send.php', {
                            method: 'POST',
                            body: data,
                        })
                        .then(resp => resp.text())
                        .then(res => {
                            countplus();
                            errorRemove();
                        });
                        
                    }
            
                    ftsend(formData);

                }  else {
                    errorAdd();
                }
            }
            if (e.target.closest('.btn-return-js')) {
                countplus();
                $modalFormQuiz.classList.remove('active');
                errorRemove();
            }

        });
    });



    $btnBlack.forEach((link, i) => {
        link.addEventListener('click', e => {
            errorRemove();
            countminus();
            
        });
      
    });
    const countplus = () => {

        if (slideGoBtn.length - 1 === stcount) {
            stcount = 0;
        } else {
            stcount++;

        }
      
        sliderModal();
    }
    
    const countminus = () => {

        stcount = stcount - 1;
        sliderModal();
    }

    const sliderModal = () => {
        for (let i = 0; i < $items.length; i++) {
         
            const element = $items[i];
            element.classList.remove('active');
            $items[stcount].classList.add('active');
        }
        
      
    }

    const errorAdd = () => {
        $itemsError.forEach(item => {
            item.classList.add('active');
        });
    }
    const errorRemove = () => {
        $itemsError.forEach(item => {
            item.classList.remove('active');
        });
    }

    sliderModal();
}


// язык

const v_langDrop = document.querySelectorAll('.lang-drop');

if (v_langDrop) {
    v_langDrop.forEach(item => {
        item.addEventListener('click', e => {
            e.preventDefault();
            item.classList.toggle('active');
        })
    })
}

const swipercontainer = new Swiper('.swiper-container', {
    loop: true,
    speed: 2000,
    longSwipesMs: 0,
    longSwipesRatio: 0.000,
    autoplay: {
        delay: 0,
        disableOnInteraction: false,
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
            centeredSlides: true,
         
        },
        340: {
            slidesPerView: 1.15,
            spaceBetween: 10,
            centeredSlides: true,
         
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1.5,
            spaceBetween: 10,
            centeredSlides: true,
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 1.5,
            spaceBetween: 30,
            centeredSlides: true,
        },
        1000: {
            slidesPerView: 2.5,
            spaceBetween: 30,
            centeredSlides: false,
            
        },
        1400: {
            slidesPerView: 3.5,
            spaceBetween: 30
        }
    }
});



const swiperContainerBlogVertical = new Swiper('.swiper-container-blog-vertical', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    mousewheel: true,
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 20,
         
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1.2,
            spaceBetween: 30,
          
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 1.5,
            spaceBetween: 30,
            
        },
        1000: {
            slidesPerView: 1.8,
            spaceBetween: 30,
            
        },
        1400: {
            slidesPerView: 2.3,
            spaceBetween: 30
        }
    }
});